const nav = document.querySelector('.menu-links');
const ul = document.createElement('ul');

const navLateral = document.querySelector('.nav-lateral');
const ulLateral = document.createElement('ul');


/* Função para criar o menu de opções do header e menu lateral */

export default async function createMenus(parentItem, child){
  await fetch('http://localhost:8888/api/V1/categories/list')
  .then(response => response.json())
  .then(data => {
    
    child.innerHTML += `<li><a href="./index.html" title="Página Inicial">Página Inicial</a></li>`;
    
    data.items.forEach(item => {
      child.innerHTML += `<li id="${item.id}"><a href="./product.html?${item.id}" title="${item.name}">${item.name}</a></li>`;
    });
    
    child.innerHTML += `<li><a title="Contato">Contato</a></li>`;
    
    parentItem.appendChild(child);
    
  });

}

createMenus(nav, ul);
createMenus(navLateral, ulLateral);