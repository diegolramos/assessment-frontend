let selectedItem = document.URL.split('?')[1];
const containerProducts = document.querySelector('.containerProducts');

/* Função para inserir alguns dados dinamicamente com informações da API */

async function createPageItemWithSelectedItem(){  
  const location = document.querySelector('.location');
  const mainTitle = document.querySelector('.main h1');
  
  await fetch(`http://localhost:8888/api/V1/categories/list`)
  .then(response => response.json())
  .then(data => {
    location.innerHTML += `${data.items[`${selectedItem - 1}`].name}`;
    mainTitle.innerHTML += `<h1>${data.items[`${selectedItem - 1}`].name}</h1>`;
  })

  await fetch(`http://localhost:8888/api/V1/categories/${selectedItem}`)
  .then(response => response.json())
  .then(data => createCardsProducts(data.items))

}

createPageItemWithSelectedItem();



/* Função para criar os cards dos produtos */

function createCardsProducts(product){
  product.forEach(item => {
    containerProducts.innerHTML += `
      <div class="card-item" id="${item.id}">
        <figure>
          <img src="../assets/${item.image}" title="${item.name}"/>
          <figcaption>${item.name.toUpperCase()}</figcaption>
        </figure>

        <div class="card-price">
          ${item.specialPrice ? `<span class="specialPrice">R$ ${item.specialPrice}</span>` : ''}
        
          <span class="normalPrice">R$ ${item.price}</span>
        </div>

        <button>COMPRAR</button>
      </div>
    `;
  })
}



/* Função para filtrar os tipos de cada item: Corrida, Caminhada, Casual e Socical */

async function filterTypes(type){
  containerProducts.innerHTML = '';
  let itemsFiltered = [];

  fetch(`http://localhost:8888/api/V1/categories/${selectedItem}`)
  .then(response => response.json())
  .then(data => {
    data.items.filter(item => {
      if(item.path.indexOf(type) > -1){
        itemsFiltered.push(item);
      }
    })
    createCardsProducts(itemsFiltered);
  })
}


/* Função para filtrar pela categoria Roupas */

async function filterCategoryClothes(){
  containerProducts.innerHTML = '';
  let itemsFiltered = [];

  await fetch(`http://localhost:8888/api/V1/categories/1`)
  .then(response => response.json())
  .then(data => data.items.forEach(item => itemsFiltered.push(item)))

  await fetch(`http://localhost:8888/api/V1/categories/2`)
  .then(response => response.json())
  .then(data => data.items.forEach(item => itemsFiltered.push(item)))
  
  createCardsProducts(itemsFiltered);
}


/* Função para filtrar pela categoria Calçados */

async function filterCategoryShoes(){
  containerProducts.innerHTML = '';
  let itemsFiltered = [];

  await fetch(`http://localhost:8888/api/V1/categories/3`)
  .then(response => response.json())
  .then(data => data.items.forEach(item => itemsFiltered.push(item)))
  
  createCardsProducts(itemsFiltered);

}